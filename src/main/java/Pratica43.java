
import utfpr.ct.dainf.if62c.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica43 {
    public static void main(String[] args) {
        
        Retangulo retangulo = new Retangulo(3,2);
        Quadrado quadrado = new Quadrado(3);
        TrianguloEquilatero triangulo = new TrianguloEquilatero(3);
        
        System.out.println("Retangulo: " + retangulo.getLadoMaior() + " x " + retangulo.getLadoMenor());
        System.out.println("Área do Retangulo: " + retangulo.getArea());
        System.out.println("Perímetro do Retangulo: " + retangulo.getPerimetro());
        System.out.println("Quadrado: " + quadrado.getLadoMaior() + " x " + quadrado.getLadoMenor());
        System.out.println("Área do Quadrado: " + quadrado.getArea());
        System.out.println("Perímetro do Quadrado: " + quadrado.getPerimetro());
        System.out.println("triangulo: " + triangulo.getLadoMaior() + " x " + quadrado.getLadoMenor());
        System.out.println("Área do triangulo: " + triangulo.getArea());
        System.out.println("Perímetro do triangulo: " + triangulo.getPerimetro());
    }
}
