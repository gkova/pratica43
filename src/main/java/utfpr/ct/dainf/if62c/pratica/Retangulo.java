/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author gabriel
 */
public class Retangulo implements FiguraComLados{
    private double l1, l2;
    
    public Retangulo(double lado1, double lado2){
        l1 = lado1;
        l2 = lado2;
    }
    
    @Override
    public double getLadoMenor() {
        return (l1<l2)?l1:l2;
    }

    @Override
    public double getLadoMaior() {
        return (l1>l2)?l1:l2;
    }
    
    @Override
    public double getArea(){
        return l1 * l2;
    }
    
    @Override
    public double getPerimetro(){
       return 2 * l1 + 2 * l2;
    }
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
}
