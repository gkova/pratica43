/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author gabriel
 */
public class TrianguloEquilatero implements FiguraComLados{
    private double l;
    
    public TrianguloEquilatero(double lado){
        l = lado;
    }
    
    @Override
    public double getArea(){
        return l * l * Math.sqrt(3.0)/4.0;
    }
    
    @Override
    public double getPerimetro(){
       return 3 * l;
    }
        
    @Override
    public double getLadoMenor() {
        return l;
    }

    @Override
    public double getLadoMaior() {
        return l;
    }
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
}
